This is a tool to create simple maps. It was created initially for a technical demonstration at ICLDC, the 6 Int. Conf. on Language Documentation & Conservation (ICLDC), Feb. 2019.

The map tool is built in Shiny and available online at: 

http://onkyo.u-aizu.ac.jp/maps/

If you want to host it, you need to obtain an API key from Google in order to download rasters of the maps.

If you find this tool useful, please cite as:

J. Villegas and S. J. Lee, “Creating maps for linguistic field-work using R,” in Proc. 6 Int. Conf. on Language Documentation & Conservation (ICLDC), Feb. 2019.

This work was supported by PhoPhono , a project of the Strategic Japanese-Swiss Science and Technology Programme of JSPS and SNSF.